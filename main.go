package main

import (
	"net/http"

	"bitbucket.org/devstartupteam/crawler/model"

	"fmt"

	"strconv"
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

type Host struct {
	Echo *echo.Echo
}

func main() {
	viper.SetConfigName("development") // name of config file (without extension)
	viper.AddConfigPath("$HOME/.trucks")          // call multiple times to add many search paths
	viper.AddConfigPath("./")          // call multiple times to add many search paths
	err := viper.ReadInConfig()        // Find and read the config file
	if err != nil {                    // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	// Hosts
	hosts := make(map[string]*Host)
	setApiSubdomain(hosts)
	setSiteDomain(hosts)
	initServer(hosts)
}

//---------
// Set
//---------
func setSiteDomain(hosts map[string]*Host) {
	//---------
	// Website
	//---------
	site := echo.New()
	setMiddlewares(site)

	siteHost := strings.Join([]string{
		viper.GetString("domain.host"),
		":",
		strconv.Itoa(viper.GetInt("domain.port")),
	}, "")

	hosts[siteHost] = &Host{site}

	site.GET("/", siteIndex)
}

func setApiSubdomain(hosts map[string]*Host) {
	//-----
	// API
	//-----
	api := echo.New()
	setMiddlewares(api)

	apiHost := strings.Join([]string{
		viper.GetString("domain.subdomains.api"),
		".",
		viper.GetString("domain.host"),
		":",
		strconv.Itoa(viper.GetInt("domain.port")),
	}, "")

	hosts[apiHost] = &Host{api}

	api.GET("/trucks", apiTrucks)
	api.GET("/", apiIndex)
}

//---------
// Middlewares
//---------
func setMiddlewares(e *echo.Echo) {
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.SecureWithConfig(middleware.SecureConfig{
		XSSProtection:         "1; mode=block",
		ContentTypeNosniff:    "nosniff",
		XFrameOptions:         "SAMEORIGIN",
		HSTSMaxAge:            3600,
		ContentSecurityPolicy: "default-src 'self'",
	}))
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))
}

//---------
// Init
//---------
func initServer(hosts map[string]*Host) {
	// Server
	e := echo.New()
	e.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		host := hosts[req.Host()]

		if host == nil {
			err = echo.ErrNotFound
		} else {
			host.Echo.ServeHTTP(req, res)
		}

		return
	})
	e.Run(standard.New(":8080"))
}

//---------
// Handlers API
//---------
func apiIndex(c echo.Context) error {
	return c.String(http.StatusOK, "API")
}

func apiTrucks(c echo.Context) error {
	t := model.GetAll("guia")

	return c.JSON(http.StatusOK, t)
}

//---------
// Handlers Site
//---------
func siteIndex(c echo.Context) error {
	return c.String(http.StatusOK, "Site")
}
