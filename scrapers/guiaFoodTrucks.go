package scrapers

import (
	"net/http"
	"regexp"
	"strings"

	"bitbucket.org/devstartupteam/crawler/lib"
	"bitbucket.org/devstartupteam/crawler/model"

	"strconv"
	"time"

	"github.com/yhat/scrape"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func GetGuia() {
	// request and parse the front page
	baseUrl := []string{"http://www.guiafoodtrucks.com"}
	baseUrl2 := []string{"http://encontre.guiafoodtrucks.com"}
	cateroriesUrl := append(baseUrl, "/categories")
	resp, err := http.Get(strings.Join(cateroriesUrl, ""))
	if err != nil {
		panic(err)
	}
	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	matcher := func(n *html.Node) bool {
		return (n.DataAtom == atom.A || n.DataAtom == atom.H1) && scrape.Attr(n.Parent, "class") == "content" && scrape.Attr(n.Parent.Parent, "class") == "section"
	}
	// grab all articles and print them
	categorias := scrape.FindAll(root, matcher)
	c := ""
	for _, categoria := range categorias {
		if categoria.DataAtom == atom.H1 {
			c = scrape.Text(categoria)
		}

		if categoria.DataAtom == atom.A {
			t := &model.Truck{}
			t.Category.Name = c
			href := scrape.Attr(categoria, "href")
			newUrl := append(baseUrl, href)
			newUrl2 := append(baseUrl2, href)
			getTrucksMenuDetails(strings.Join(newUrl2, ""), t)
			getTrucksDetails(strings.Join(newUrl, ""), t)

			t.Print()
			t.Save("final1")
		}
	}
}

func getTrucksMenuDetails(url string, t *model.Truck) {
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	matcher := func(n *html.Node) bool {
		return scrape.Attr(n, "class") == "cardapio"
	}

	info, ok := scrape.Find(root, matcher)

	if ok {
		t.Menu = scrape.TextJoin(info, lib.Join)
	}
}

func getTrucksDetails(url string, t *model.Truck) {
	bgRegex := regexp.MustCompile(`^background-image: url\('(.*)'\);$`)

	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	matcher := func(n *html.Node) bool {
		return n.DataAtom == atom.Div && scrape.Attr(n, "class") == "single-truck"
	}

	info, ok := scrape.Find(root, matcher)

	if ok {
		header, headerOK := scrape.Find(info, scrape.ByClass("t-header"))
		about, aboutOK := scrape.Find(info, scrape.ByClass("t-about"))
		agenda, agendaOK := scrape.Find(info, scrape.ByClass("t-agenda"))
		menu := scrape.FindAll(info, scrape.ByClass("t-menu-item"))
		contact, contactOK := scrape.Find(info, scrape.ByClass("t-contact"))
		pay, payOK := scrape.Find(info, scrape.ByClass("t-pay"))

		// header
		if headerOK {
			name, _ := scrape.Find(header, scrape.ByTag(atom.H1))
			cDescription, _ := scrape.Find(header, scrape.ByTag(atom.H2))
			logo, _ := scrape.Find(header, scrape.ByTag(atom.Img))

			t.Name = scrape.Text(name)
			t.Pictures.Main = bgRegex.FindStringSubmatch(scrape.Attr(header, "style"))[1]
			t.Category.Description = scrape.Text(cDescription)
			t.Pictures.Logo = scrape.Attr(logo, "src")
		}

		// about
		if aboutOK {
			t.Description = scrape.Text(about)
		}

		// agenda
		if agendaOK {
			calendar := scrape.FindAll(agenda, scrape.ByTag(atom.A))

			for _, c := range calendar {
				when := scrape.FindAll(c, func(n *html.Node) bool {
					return n.DataAtom == atom.Span && scrape.Attr(n.Parent.Parent.Parent, "class") == "when"
				})

				cal := model.Calendar{}
				cal.When.Date = scrape.Text(when[0]) + "/2016"
				cal.When.Start = scrape.Text(when[1])
				cal.When.End = scrape.Text(when[2])

				where := scrape.FindAll(c, func(n *html.Node) bool {
					return n.DataAtom == atom.Span && scrape.Attr(n.Parent.Parent.Parent, "class") == "where"
				})

				cal.Where.Place = scrape.Text(where[0])
				cal.Where.Address = scrape.Text(where[1])
				cal.Where.MapsLink = scrape.Attr(c, "href")

				dateStr := strings.Split(scrape.Text(when[0]), "/")
				day, _ := strconv.Atoi(dateStr[0])
				month, _ := strconv.Atoi(dateStr[1])
				year := 2016
				timeStr := strings.Split(scrape.Text(when[1]), ":")
				hour, _ := strconv.Atoi(timeStr[0])
				min, _ := strconv.Atoi(timeStr[1])

				cal.Date = time.Date(year, time.Month(month), day, hour, min, 0, 0, time.UTC)

				t.Calendar = append(t.Calendar, cal)
			}
		}

		// menu -> Pictures.Trucks
		for _, item := range menu {
			i := scrape.Attr(item, "style")
			i = bgRegex.FindStringSubmatch(i)[1]

			t.Pictures.Truck = append(t.Pictures.Truck, i)
		}

		// contact
		if contactOK {
			contacts := scrape.FindAll(contact, scrape.ByTag(atom.A))

			for _, cs := range contacts {
				href := scrape.Attr(cs, "href")
				cst, csTypeOK := scrape.Find(cs, scrape.ByClass("content"))
				csType := scrape.Text(cst)
				telRegex, _ := regexp.MatchString("^Ligue.*", csType)
				emailRegex, _ := regexp.MatchString("^Enviar e-mail.*", csType)

				if csTypeOK {
					switch csType {
					case "Instagram":
						t.Contact.Instagram = href
					case "Visitar perfil no Facebook":
						t.Contact.Facebook = href
					case "Visite nosso site":
						t.Contact.Site = href
					}
				}

				if telRegex {
					tel := regexp.MustCompile("^tel://(.*)")
					t.Contact.Phone = append(t.Contact.Phone, tel.FindStringSubmatch(href)[1])
				}

				if emailRegex {
					tel := regexp.MustCompile("^mailto:(.*)")
					t.Contact.Email = append(t.Contact.Email, tel.FindStringSubmatch(href)[1])
				}
			}
		}

		if payOK {
			items := scrape.FindAll(pay, scrape.ByClass("item"))
			for _, item := range items {
				pt, _ := scrape.Find(item, scrape.ByTag(atom.P))
				payType := scrape.Text(pt)
				payRegex := regexp.MustCompile(`.*/(.*)\..*`)

				payImgs := scrape.FindAll(item, scrape.ByTag(atom.Img))
				for _, pi := range payImgs {
					switch payType {
					case "Crédito":
						bandeira := payRegex.FindStringSubmatch(scrape.Attr(pi, "src"))[1]
						t.Payment.Credit = append(t.Payment.Credit, bandeira)
					case "Débito":
						bandeira := payRegex.FindStringSubmatch(scrape.Attr(pi, "src"))[1]
						t.Payment.Debit = append(t.Payment.Debit, bandeira)
					case "Vale Refeição":
						bandeira := payRegex.FindStringSubmatch(scrape.Attr(pi, "src"))[1]
						t.Payment.Ticket = append(t.Payment.Ticket, bandeira)
					}
				}
			}
		}
	} // if ok
}
