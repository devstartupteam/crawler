package scrapers

import (
	"net/http"

	"bitbucket.org/devstartupteam/crawler/lib"
	"bitbucket.org/devstartupteam/crawler/model"

	"github.com/yhat/scrape"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"

	"regexp"
	"strings"
)

var baseUrl = "http://www.foodtrucknasruas.com.br/"

func GetNasRuas() {
	catalogUrls := getCatalogUrls()

	scrapeUrl(catalogUrls)
}

func getCatalogUrls() []string {
	catalogUrls := []string{}
	catalogUrl := "http://www.foodtrucknasruas.com.br/encontre-um-food-truck.php?1=1&ds_state=SP&ds_city=S%C3%A3o%20Paulo#foodTruckSelect"

	resp, err := http.Get(catalogUrl)
	if err != nil {
		panic(err)
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	matcher := func(n *html.Node) bool {
		return n.DataAtom == atom.A && n.Parent.DataAtom == atom.Div && scrape.Attr(n.Parent, "class") == "boxfoodtruck"
	}

	items := scrape.FindAll(root, matcher)

	for _, i := range items {
		link := baseUrl + scrape.Attr(i, "href")
		catalogUrls = append(catalogUrls, link)
	}

	return catalogUrls
}

func scrapeUrl(urls []string) {
	for _, myUrl := range urls {
		resp, err := http.Get(lib.ClearSpaceUrl(myUrl))
		if err != nil {
			panic(err)
		}

		root, err := html.Parse(resp.Body)
		if err != nil {
			panic(err)
		}

		matcher := func(n *html.Node) bool {
			return n.DataAtom == atom.Section && scrape.Attr(n, "class") == "miolo"
		}

		page, ok := scrape.Find(root, matcher)
		if ok {
			t := &model.Truck{}
			left, leftOK := scrape.Find(page, scrape.ByClass("lateral_esquerda"))
			right, rightOK := scrape.Find(page, scrape.ByClass("lateral_direita"))

			if leftOK {
				logo, logoOK := scrape.Find(left, scrape.ByClass("logotipo_empresa"))
				if logoOK {
					t.Pictures.Logo = baseUrl + lib.ClearSpaceUrl(scrape.Attr(logo.FirstChild, "src"))
				}

				contacts := scrape.FindAll(left, scrape.ByClass("infos_lateral_foodtruck"))
				for _, c := range contacts {
					switch scrape.Text(c.FirstChild) {
					case "Site:":
						t.Contact.Site = scrape.Attr(c.LastChild.FirstChild, "href")
					case "Facebook:":
						fbRegex := regexp.MustCompile("(.*)facebook(.*)")
						t.Contact.Facebook = fbRegex.ReplaceAllString(scrape.Attr(c.LastChild.FirstChild, "href"), "${1}fb${2}")
					case "Instagram:":
						t.Contact.Instagram = scrape.Attr(c.LastChild.FirstChild, "href")
					case "Telefone:":
						telRegex := regexp.MustCompile(`.*(\(.{13,15})$`)
						t.Contact.Phone = append(t.Contact.Phone, telRegex.ReplaceAllString(scrape.Text(c), "${1}"))
					case "E-mail:":
						t.Contact.Email = getEmail(baseUrl + scrape.Attr(c.LastChild.FirstChild, "href"))
					}
				} // end contacts

				pay := scrape.FindAll(left, func(n *html.Node) bool {
					return n.DataAtom == atom.Img && scrape.Attr(n.Parent, "class") == "formas_pagamento"
				})
				for _, p := range pay {
					text := scrape.Attr(p, "alt")
					if text == "Dinheiro" {
						t.Payment.Money = true
					} else {
						t.Payment.Credit = append(t.Payment.Credit, text)
						t.Payment.Debit = append(t.Payment.Debit, text)
						t.Payment.Ticket = append(t.Payment.Ticket, text)
					}
				} // end payment

				pics := scrape.FindAll(left, func(n *html.Node) bool {
					return n.DataAtom == atom.A && scrape.Attr(n.Parent, "class") == "foto_miniatura"
				})
				for _, pic := range pics {
					t.Pictures.Truck = append(t.Pictures.Truck, baseUrl+lib.ClearSpaceUrl(scrape.Attr(pic, "href")))
				} // end pics truck
			} // if leftOK

			if rightOK {
				name, nameOK := scrape.Find(right, scrape.ByClass("linktruck"))
				if nameOK {
					t.Name = scrape.Text(name)
				}

				ps := scrape.FindAll(right, scrape.ByTag(atom.P))

				isMenuStart := false
				isMenuEnd := false
				menu := []string{}
				for _, p := range ps {
					reg := regexp.MustCompile(`(.*:).*`)
					txt := reg.ReplaceAllString(scrape.Text(p), "${1}")
					switch txt {
					case "√ Food Truck especializado em:":
						t.Category.Description = strings.Replace(scrape.Text(p), "√ Food Truck especializado em:", "", -1)
					case "√ Ponto fixo:":
						t.Address = strings.Replace(scrape.Text(p), "√ Ponto fixo:", "", 1)
					case "√ Cardápio oferecido:":
						isMenuStart = true
						isMenuEnd = true
					case "...........................................................................................................................................................................................................":
						if isMenuStart {
							isMenuEnd = true
						}
					default:
						isMenuEnd = false
					}

					if (strings.IndexAny(txt, "√") == 0 || strings.IndexAny(txt, "*") == 0) && isMenuStart {
						isMenuEnd = true
					}

					if isMenuStart && !isMenuEnd {
						menu = append(menu, scrape.Text(p))
					}
				}
				t.Menu = lib.Join(menu)
			} // if rightOK
			t.Print()
			t.Save("final1")
		} // if ok
	}
}

func getEmail(link string) []string {
	emails := []string{}
	resp, err := http.Get(link)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	root, err := html.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	matcher := func(n *html.Node) bool {
		return scrape.Attr(n, "id") == "ds_ftruck_email"
	}

	page, ok := scrape.Find(root, matcher)

	if ok {
		emails = append(emails, scrape.Attr(page, "value"))
	}

	return emails
}
