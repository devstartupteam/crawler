package lib

import (
	"strings"
)

func ClearSpaceUrl(u string) string {
	return strings.Replace(u, " ", "%20", -1)
}

func Join(s []string) string {
	ss := strings.Join(s, "\n")
	return ss
}
