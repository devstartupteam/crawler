package model

import (
	"encoding/json"
	"fmt"
	"log"

	"time"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Truck struct {
	Name        string     `json:"name" form:"name"`
	Description string     `json:"description" form:"description"`
	Address     string     `json:"address" form:"address"`
	Menu        string     `json:"menu" form:"menu"`
	Category    Category   `json:"category" form:"category"`
	Pictures    Pictures   `json:"pictures" form:"pictures"`
	Contact     Contact    `json:"contact" form:"contact"`
	Payment     Payment    `json:"payment" form:"payment"`
	Calendar    []Calendar `json:"calendar" form:"calendar"`
}

type Category struct {
	Name        string `json:"name" form:"name"`
	Description string `json:"description" form:"description"`
}

type Pictures struct {
	Logo  string   `json:"logo" form:"logo"`
	Main  string   `json:"main" form:"main"`
	Truck []string `json:"truck" form:"truck"`
}

type Contact struct {
	Email     []string `json:"email" form:"email"`
	Phone     []string `json:"phone" form:"phone"`
	Site      string   `json:"site" form:"site"`
	Facebook  string   `json:"facebook" form:"facebook"`
	Instagram string   `json:"instagram" form:"instagram"`
}

type Payment struct {
	Debit  []string `json:"debit" form:"debit"`
	Credit []string `json:"credit" form:"credit"`
	Ticket []string `json:"ticket" form:"ticket"`
	Money  bool     `json:"money" form:"money"`
}

type Calendar struct {
	Date  time.Time `json:"date" form:"date"`
	When  When      `json:"when" form:"when"`
	Where Where     `json:"where" form:"where"`
}

type When struct {
	Date  string `json:"date" form:"date"`
	Start string `json:"start" form:"start"`
	End   string `json:"end" form:"end"`
}

type Where struct {
	Place    string `json:"place" form:"place"`
	Address  string `json:"address" form:"address"`
	MapsLink string `json:"mapsLink" form:"mapsLink"`
}

func getConnection() *mgo.Database {
	mongoUrl := viper.GetString("database.mongodb.host")
	mongoDB := viper.GetString("database.mongodb.dbName")

	session, err := mgo.Dial(mongoUrl)
	if err != nil {
		panic(err)
	}
	//defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	return session.DB(mongoDB)
}

func GetAll(collection string) []Truck {
	db := getConnection()
	c := db.C(collection)

	t := []Truck{}
	err := c.
		Find(nil).
		Select(bson.M{
			"name":          1,
			"description":   1,
			"category.name": 1,
			"contact":       1,
			"menu":          1,
		}).
		Sort("contact.email", "name").
		All(&t)

	if err != nil {
		log.Fatal(err)
	}

	return t
}

func GetAllInfo(collection string) []Truck {
	db := getConnection()
	c := db.C(collection)

	t := []Truck{}
	err := c.Find(nil).Sort("contact.email", "name").All(&t)
	if err != nil {
		log.Fatal(err)
	}

	return t
}

func (t Truck) Save(collection string) {
	db := getConnection()

	c := db.C(collection)
	err := c.Insert(t)
	if err != nil {
		log.Fatal(err)
	}
}

func (t Truck) Print() {
	b, err := json.Marshal(t)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(b))
}
